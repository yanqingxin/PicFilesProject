package com.ctrip.flight.test.piccompare.picselect;

import com.ctrip.flight.framework.soa.entities.ServiceContext;
import com.ctrip.flight.framework.soa.entities.ServiceHandler;
import com.ctrip.flight.framework.utility.serialization.JsonSerializer;
import com.ctrip.flight.test.connection.SimpleConnetionPool;
import com.ctrip.flight.test.piccompare.common.ConfigKeys;
import com.ctrip.flight.test.piccompare.common.ConfigValues;
import com.ctriposs.baiji.rpc.common.types.AckCodeType;
import com.ctriposs.baiji.rpc.common.types.ErrorDataType;
import com.ctriposs.baiji.rpc.common.types.ResponseStatusType;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by yanqx on 2017/8/24.
 */
public class PicSelectHandler implements ServiceHandler<PicSelectResponseType>{
    @Override
    public PicSelectResponseType process(ServiceContext serviceContext) throws Exception {
        PicSelectRequestType request = (PicSelectRequestType)serviceContext.getRequest();
        PicSelectResponseType response = new PicSelectResponseType();
        String groupId = request.getGroupId();
        String pageName = request.getPageName();
        ResponseStatusType status = new ResponseStatusType();
        //初始化需要的配置
        ConfigValues initConfig = new ConfigValues(ConfigKeys.SELECT_ROWS);
        int checkRows = Integer.parseInt(ConfigValues.configs.get(ConfigKeys.SELECT_ROWS));

        ArrayList<PicPage> pageList = new ArrayList<>();
        String resultMessage = "";
        if (groupId==null||groupId.length()==0){
            return getErrorResponse("please give me the groupId!");
        }else if(pageName==null||pageName.length()==0){
            return getErrorResponse("please give me the pageName!");
        }

        new SimpleConnetionPool("jdbc:sqlserver://10.2.33.162\\TestAutoServer:8765;databaseName=FlightBookingAutomation","sa","Admin_001");
        Connection conn = SimpleConnetionPool.getConnection();
        Statement stmt = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String selectSql = "select top "+checkRows+" transportId,timestamp,status from FlightBookingAutomation..ImageTransport where pageName=? and groupid=?";
        try {
            if (conn!=null) {
                pstmt = conn.prepareStatement(selectSql);
                pstmt.setString(1,pageName);
                pstmt.setString(2,groupId);
                rs = pstmt.executeQuery();
                    while (rs.next()){
                        PicPage page = new PicPage();
                        page.setGroupId(groupId);
                        page.setPageName(pageName);
                        page.setTrancseportID(rs.getString(1));
                        page.setTimestamp(rs.getString(2));
                        page.setStatus(rs.getString(3));
                        pageList.add(page);
                    }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            SimpleConnetionPool.pushConnectionBackToPool(conn);
            SimpleConnetionPool.close();
        }

        if(pageList.size()>0){
           resultMessage =  JsonSerializer.toJson(pageList);
        }else{
            return getErrorResponse("There is no data for this page !");
        }
        status.setAck(AckCodeType.Success);
        response.setResponseStatus(status);
        response.setPagesMessage(resultMessage);
        return  response;

    }

    //设置ErrorMessage并返回response
    public PicSelectResponseType getErrorResponse(String errorMess){
        PicSelectResponseType response = new PicSelectResponseType();
        ResponseStatusType status = new ResponseStatusType();
        ArrayList<ErrorDataType> errors = new ArrayList<ErrorDataType>();
        status.setAck(AckCodeType.Failure);
        ErrorDataType errorData = new ErrorDataType();
        errorData.setMessage(errorMess);
        errors.add(errorData);
        status.setErrors(errors);
        response.setResponseStatus(status);
        response.setPagesMessage(errorMess);
        return response;
    }
}
