package com.ctrip.flight.test.piccompare.picrefresh;

import com.ctrip.flight.framework.soa.entities.ServiceContext;
import com.ctrip.flight.framework.soa.entities.ServiceHandler;
import com.ctrip.flight.test.piccompare.common.ConfigValues;
import com.ctriposs.baiji.rpc.common.types.AckCodeType;
import com.ctriposs.baiji.rpc.common.types.ErrorDataType;
import com.ctriposs.baiji.rpc.common.types.ResponseStatusType;

import java.util.ArrayList;

/**
 * Created by yanqx on 2017/8/23.
 */
public class PicRefreshHandler implements ServiceHandler<PicRefreshResponseType> {
    @Override
    public PicRefreshResponseType process(ServiceContext serviceContext) throws Exception {
        PicRefreshRequestType request = (PicRefreshRequestType)serviceContext.getRequest();
        PicRefreshResponseType response = new PicRefreshResponseType();
        ResponseStatusType status = new ResponseStatusType();

        String key = request.getConfigKey();
        ConfigValues configValues = new ConfigValues();
        boolean configSuccess = configValues.setAndrefresh(key);
        if (configSuccess){
            String value = configValues.configs.get(key);
            if (value==null||value.length()==0){
                return getErrorResponse("the value of this key is null , please check your Qconfig:http://qconfig.ctripcorp.com/view/currentPublish.do；appid:100009724");
            }
        }else{
            return getErrorResponse("the config refresh failed!");
        }
        status.setAck(AckCodeType.Success);
        response.setResponseStatus(status);
        response.setRefreshResult("Refresh Success!,the value of key : "+key+" is "+configValues.configs.get(key));
        return response;
    }

    //设置ErrorMessage并返回response
    public PicRefreshResponseType getErrorResponse(String errorMess){
        PicRefreshResponseType response = new PicRefreshResponseType();
        ResponseStatusType status = new ResponseStatusType();
        ArrayList<ErrorDataType> errors = new ArrayList<ErrorDataType>();
        status.setAck(AckCodeType.Failure);
        ErrorDataType errorData = new ErrorDataType();
        errorData.setMessage(errorMess);
        errors.add(errorData);
        status.setErrors(errors);
        response.setResponseStatus(status);
        response.setRefreshResult(errorMess);
        return response;
    }
}
