package com.ctrip.flight.test.piccompare.picselect;

/**
 * Created by yanqx on 2017/8/25.
 */
public class PicPage {
    private String pageName;
    private String groupId;
    private String trancseportID;
    private String timestamp;
    private String Status;
    public String getTrancseportID() {
        return trancseportID;
    }
    public void setTrancseportID(String trancseportID) {
        this.trancseportID = trancseportID;
    }
    public String getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    public String getStatus() {
        return Status;
    }
    public void setStatus(String status) {
        Status = status;
    }
    public String getPageName() {
        return pageName;
    }
    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
