package com.ctrip.flight.test.piccompare.serverbase;

import com.ctriposs.baiji.rpc.server.BaijiListener;
import com.ctriposs.baiji.rpc.server.HostConfig;

import javax.servlet.annotation.WebListener;

@WebListener
public class BaijiWebListener extends BaijiListener {
    public BaijiWebListener(){
	    super(PicCompareServiceImpl.class);
	}

    @Override
    protected void configure(final HostConfig hostConfig) {

    }
}