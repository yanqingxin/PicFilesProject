package com.ctrip.flight.test.piccompare.common;

import com.ctrip.flight.framework.utility.config.ConfigManager;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * Created by yanqx on 2017/8/23.
 */
public class ConfigValues {

    public static  Hashtable<String,String>configs = new Hashtable<>();

    //构造放入所需配置
    public ConfigValues(String key){
        if (!configs.containsKey(key)){setAndrefresh(key);}
    }
    public ConfigValues(){
    }

    //根据具体key值update配置，否则update 所有配置，或者新增某个配置
    public boolean setAndrefresh(String key) {

        if (key == null || key.length() == 0) {
            Enumeration<String> keys = configs.keys();
            while (keys.hasMoreElements()) {
                String currentKey = keys.nextElement();
                String curretValue = configs.get(currentKey);
                System.out.println("Old: key:" + currentKey + "--value:" + curretValue);
                String newValue = getConfigValue(currentKey);
                //超时熔断会返回null
                if (newValue==null||newValue.length()==0){
                    newValue = "";
                }
                configs.put(currentKey, newValue);
                System.out.println("New: key:" + currentKey + "--value:" + configs.get(currentKey));
            }
            return true;
        }
        try {
            String configValue = getConfigValue(key);
            if (configValue == null ||configValue.length()==0){
                configs.put(key,"");
            }else{
                configs.put(key,configValue);
            }
            System.out.println("New: key:" + key + "--value:" + configs.get(key));

        } catch (Exception e) {
            return  false;
        }
        return true;
    }

    //取到QConfig配置
    private String getConfigValue(String key){
        return ConfigManager.getConfig().getProperty(key);
    }

}
