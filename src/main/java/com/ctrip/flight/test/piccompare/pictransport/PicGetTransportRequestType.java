package com.ctrip.flight.test.piccompare.pictransport;

import com.ctriposs.baiji.exception.*;
import com.ctriposs.baiji.rpc.common.*;
import com.ctriposs.baiji.schema.*;
import com.ctriposs.baiji.specific.*;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.Objects;
import com.google.common.base.MoreObjects;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ctriposs.baiji.rpc.mobile.common.types.MobileRequestHead;
import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("all")
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE) 
@JsonPropertyOrder({
    "head",
    "pageName",
    "groupId"
})
public class PicGetTransportRequestType implements SpecificRecord, HasMobileRequestHead {
    private static final long serialVersionUID = 1L;

	@JsonIgnore
    public static final Schema SCHEMA = Schema.parse("{\"type\":\"record\",\"name\":\"PicGetTransportRequestType\",\"namespace\":\"" + PicGetTransportRequestType.class.getPackage().getName() + "\",\"doc\":null,\"fields\":[{\"name\":\"head\",\"type\":[{\"type\":\"record\",\"name\":\"MobileRequestHead\",\"namespace\":\"com.ctriposs.baiji.rpc.mobile.common.types\",\"doc\":null,\"fields\":[{\"name\":\"syscode\",\"type\":[\"string\",\"null\"]},{\"name\":\"lang\",\"type\":[\"string\",\"null\"]},{\"name\":\"auth\",\"type\":[\"string\",\"null\"]},{\"name\":\"cid\",\"type\":[\"string\",\"null\"]},{\"name\":\"ctok\",\"type\":[\"string\",\"null\"]},{\"name\":\"cver\",\"type\":[\"string\",\"null\"]},{\"name\":\"sid\",\"type\":[\"string\",\"null\"]},{\"name\":\"extension\",\"type\":[{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"ExtensionFieldType\",\"namespace\":\"com.ctriposs.baiji.rpc.mobile.common.types\",\"doc\":null,\"fields\":[{\"name\":\"name\",\"type\":[\"string\",\"null\"]},{\"name\":\"value\",\"type\":[\"string\",\"null\"]}]}},\"null\"]},{\"name\":\"sauth\",\"type\":[\"string\",\"null\"]}]},\"null\"]},{\"name\":\"pageName\",\"type\":[\"string\",\"null\"]},{\"name\":\"groupId\",\"type\":[\"string\",\"null\"]}]}");

    @Override
    @JsonIgnore
    public Schema getSchema() { return SCHEMA; }

    public PicGetTransportRequestType(
        MobileRequestHead head,
        String pageName,
        String groupId) {
        this.head = head;
        this.pageName = pageName;
        this.groupId = groupId;
    }

    public PicGetTransportRequestType() {
    }

    @JsonProperty("head") 
    public MobileRequestHead head;


    @JsonProperty("pageName") 
    public String pageName;


    @JsonProperty("groupId") 
    public String groupId;

    @JsonProperty("head") 
    public MobileRequestHead getHead() {
        return head;
    }

    @JsonProperty("head") 
    public void setHead(final MobileRequestHead head) {
        this.head = head;
    }


    @JsonProperty("pageName") 
    public String getPageName() {
        return pageName;
    }

    @JsonProperty("pageName") 
    public void setPageName(final String pageName) {
        this.pageName = pageName;
    }


    @JsonProperty("groupId") 
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("groupId") 
    public void setGroupId(final String groupId) {
        this.groupId = groupId;
    }

    // Used by DatumWriter. Applications should not call.
    public Object get(int fieldPos) {
        switch (fieldPos) {
            case 0: return this.head;
            case 1: return this.pageName;
            case 2: return this.groupId;
            default: throw new BaijiRuntimeException("Bad index " + fieldPos + " in get()");
        }
    }

    // Used by DatumReader. Applications should not call.
    @SuppressWarnings(value="unchecked")
    public void put(int fieldPos, Object fieldValue) {
        switch (fieldPos) {
            case 0: this.head = (MobileRequestHead)fieldValue; break;
            case 1: this.pageName = (String)fieldValue; break;
            case 2: this.groupId = (String)fieldValue; break;
            default: throw new BaijiRuntimeException("Bad index " + fieldPos + " in put()");
        }
    }

    @Override
	public Object get(String fieldName) {
        Schema schema = getSchema();
        if (!(schema instanceof RecordSchema)) {
            return null;
        }
        Field field = ((RecordSchema) schema).getField(fieldName);
        return field != null ? get(field.getPos()) : null;
    }

    @Override
    public void put(String fieldName, Object fieldValue) {
        Schema schema = getSchema();
        if (!(schema instanceof RecordSchema)) {
            return;
        }
        Field field = ((RecordSchema) schema).getField(fieldName);
        if (field != null) {
            put(field.getPos(), fieldValue);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        final PicGetTransportRequestType other = (PicGetTransportRequestType)obj;
        return 
            Objects.equal(this.head, other.head) &&
            Objects.equal(this.pageName, other.pageName) &&
            Objects.equal(this.groupId, other.groupId);
    }

    @Override
    public int hashCode() {
        int result = 1;

        result = 31 * result + (this.head == null ? 0 : this.head.hashCode());
        result = 31 * result + (this.pageName == null ? 0 : this.pageName.hashCode());
        result = 31 * result + (this.groupId == null ? 0 : this.groupId.hashCode());

        return result;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("head", head)
            .add("pageName", pageName)
            .add("groupId", groupId)
            .toString();
    }
}
