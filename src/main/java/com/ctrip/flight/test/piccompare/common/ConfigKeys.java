package com.ctrip.flight.test.piccompare.common;

/**
 * Created by yanqx on 2017/8/23.
 */
public class ConfigKeys {
    /*FTP路径地址*/
    public final static String FTP_PATH = "Pic.UploadPath";
    /*查询接口查询条数*/
    public final static String SELECT_ROWS = "Pic.SelectRows";
}
