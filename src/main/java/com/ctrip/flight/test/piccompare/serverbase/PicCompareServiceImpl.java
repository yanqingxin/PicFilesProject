package com.ctrip.flight.test.piccompare.serverbase;
import com.ctrip.flight.framework.log.LogHelper;
import com.ctrip.flight.framework.soa.service.ServiceProcessAgent;
import com.ctrip.flight.test.piccompare.picrefresh.PicRefreshRequestType;
import com.ctrip.flight.test.piccompare.picrefresh.PicRefreshResponseType;
import com.ctrip.flight.test.piccompare.picrefresh.PicRefreshHandler;
import com.ctrip.flight.test.piccompare.picselect.PicSelectHandler;
import com.ctrip.flight.test.piccompare.picselect.PicSelectRequestType;
import com.ctrip.flight.test.piccompare.picselect.PicSelectResponseType;
import com.ctrip.flight.test.piccompare.picsortout.PicSortOutHandler;
import com.ctrip.flight.test.piccompare.picsortout.PicSortOutRequestType;
import com.ctrip.flight.test.piccompare.picsortout.PicSortOutResponseType;
import com.ctrip.flight.test.piccompare.pictransport.PicGetTransportHandler;
import com.ctrip.flight.test.piccompare.pictransport.PicGetTransportRequestType;
import com.ctrip.flight.test.piccompare.pictransport.PicGetTransportResponseType;
import com.ctriposs.baiji.rpc.common.types.CheckHealthResponseType;
import com.ctriposs.baiji.rpc.common.types.CheckHealthRequestType;

import java.util.ArrayList;
import java.util.Iterator;

public class PicCompareServiceImpl implements PicCompareService {

    @Override
    public PicGetTransportResponseType picGetTransport(PicGetTransportRequestType request) throws Exception {
        return ServiceProcessAgent.process(request,PicGetTransportHandler.class,PicGetTransportResponseType.class);
    }

    @Override
    public PicSortOutResponseType picSortOut(PicSortOutRequestType request) throws Exception {
        return  ServiceProcessAgent.process(request,PicSortOutHandler.class,PicSortOutResponseType.class);
    }

    @Override
    public PicSelectResponseType picSelect(PicSelectRequestType request) throws Exception {
        return ServiceProcessAgent.process(request,PicSelectHandler.class,PicSelectResponseType.class);
    }

    @Override
    public PicRefreshResponseType picRefresh(PicRefreshRequestType request) throws Exception {
        return ServiceProcessAgent.process(request,PicRefreshHandler.class,PicRefreshResponseType.class);
    }

    @Override
    public CheckHealthResponseType checkHealth(CheckHealthRequestType request) throws Exception {
        return new CheckHealthResponseType();
    }
    //测试主函数
    public static  void main(String[] args){
        PicCompareServiceImpl serviceImpl = new PicCompareServiceImpl();
        testselectResponse(serviceImpl);
        //testTransportResponse(serviceImpl);
        //testSortOutResponse(serviceImpl,"4c588286efe5486a8c320ca90d53ca3d");
    }
    //测试tranceport接口
    public static void  testselectResponse(PicCompareServiceImpl serviceImpl){
        PicSelectRequestType request = new PicSelectRequestType();
        request.setPageName("详情页");
        request.setGroupId("1");
        try {
            PicSelectResponseType response = serviceImpl.picSelect(request);
            LogHelper.info("PicGetBatchRequest",com.ctrip.flight.framework.utility.serialization.JsonSerializer.toJson(request));
            LogHelper.info("PicGetBatchResponse",com.ctrip.flight.framework.utility.serialization.JsonSerializer.toJson(response));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    //测试tranceport接口
    public static void  testSortOutResponse(PicCompareServiceImpl serviceImpl,String id){
        PicSortOutRequestType request = new PicSortOutRequestType();
        request.setTransportId(id);
        try {
            PicSortOutResponseType response = serviceImpl.picSortOut(request);
            LogHelper.info("PicGetBatchRequest",com.ctrip.flight.framework.utility.serialization.JsonSerializer.toJson(request));
            LogHelper.info("PicGetBatchResponse",com.ctrip.flight.framework.utility.serialization.JsonSerializer.toJson(response));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    //测试tranceport接口
    public static void  testTransportResponse(PicCompareServiceImpl serviceImpl){
        PicGetTransportRequestType request = new PicGetTransportRequestType();
        request.setPageName("详情页");
        request.setGroupId("1");
        try {
            PicGetTransportResponseType response = serviceImpl.picGetTransport(request);
            LogHelper.info("PicGetBatchRequest",com.ctrip.flight.framework.utility.serialization.JsonSerializer.toJson(request));
            LogHelper.info("PicGetBatchResponse",com.ctrip.flight.framework.utility.serialization.JsonSerializer.toJson(response));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    //测试tranceport接口
    public static void  testRefreshResponse(PicCompareServiceImpl serviceImpl){
        PicRefreshRequestType request = new PicRefreshRequestType();
        request.setConfigKey("Pic.UploadPath");
        try {
            PicRefreshResponseType response = serviceImpl.picRefresh(request);
            LogHelper.info("PicRefreshResponseType",com.ctrip.flight.framework.utility.serialization.JsonSerializer.toJson(request));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    //方法测试代码,测试迭代器的遍历删除方法
    public static void interatorTest(){
        ArrayList<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        Iterator interator = list.iterator();
        while (interator.hasNext()){
            String s = interator.next().toString();
            System.out.println(s);
            if (s.equals("2")){
                interator.remove();
            }
        }
        System.out.println("``result```");
        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }
}