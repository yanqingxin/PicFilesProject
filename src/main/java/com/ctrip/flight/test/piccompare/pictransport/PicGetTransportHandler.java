package com.ctrip.flight.test.piccompare.pictransport;

import com.ctrip.flight.framework.soa.entities.Service;
import com.ctrip.flight.framework.soa.entities.ServiceContext;
import com.ctrip.flight.framework.soa.entities.ServiceHandler;
import com.ctrip.flight.test.connection.SimpleConnetionPool;
import com.ctrip.flight.test.piccompare.common.ConfigKeys;
import com.ctrip.flight.test.piccompare.common.ConfigValues;
import com.ctriposs.baiji.rpc.common.types.AckCodeType;
import com.ctriposs.baiji.rpc.common.types.ErrorDataType;
import com.ctriposs.baiji.rpc.common.types.ResponseStatusType;

import java.io.File;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.UUID;
import java.util.Date;
/**
 * Created by yanqx on 2017/8/9.
 */
@Service(serviceCode = "13629")
public class PicGetTransportHandler implements ServiceHandler<PicGetTransportResponseType>{
    @Override
    public PicGetTransportResponseType process(ServiceContext serviceContext) throws Exception {
        PicGetTransportRequestType request=(PicGetTransportRequestType)serviceContext.getRequest();
        PicGetTransportResponseType response = new PicGetTransportResponseType();
        ResponseStatusType status = new ResponseStatusType();

        String pageName = request.getPageName();
        String groupID = request.getGroupId();
        String transportID = getGuid();
        //初始化需要的配置
        ConfigValues initConfig = new ConfigValues(ConfigKeys.FTP_PATH);
        String dirField = ConfigValues.configs.get(ConfigKeys.FTP_PATH);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String timeStamp = df.format(new Date());
        //request合法性验证
         if (pageName==null||pageName.length()==0){
            response = getErrorResponse("illegal request Type：The pageName cannot be null ");
             return response;
         } else if(groupID==null||groupID.length()==0){
             response = getErrorResponse("illegal request Type：The groupID cannot be null ");
             return response;
         }

         //合法请求入库
        try{
            new SimpleConnetionPool("jdbc:sqlserver://10.2.33.162\\TestAutoServer:8765;databaseName=FlightBookingAutomation","sa","Admin_001");
            Connection conn = SimpleConnetionPool.getConnection();
            Statement stmt = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            String insertSql = "insert into FlightBookingAutomation..ImageTransport (pageName,transportId,groupId,timestamp)values(?,?,?,?)";

            System.out.println("获取配置path："+dirField);
            if (!dirField.endsWith(File.separator)){
                dirField = dirField+File.separator;
                System.out.println("修复后的配置path："+dirField);
            }
            try {
                if (conn!=null) {
                    pstmt = conn.prepareStatement(insertSql);
                    pstmt.setString(1, pageName);
                    pstmt.setString(2,transportID);
                    pstmt.setString(3,groupID);
                    pstmt.setString(4,timeStamp);
                    int resultInt= pstmt.executeUpdate();
                    if (resultInt==0){
                        response = getErrorResponse("insert dataBase fail ! Checkout the dataBase!");
                        return response;
                    }else{
                    //新建一个指定文件夹
                        if(!createDir(dirField+transportID)){
                            response = getErrorResponse("create new dir failed!");
                            return response;
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }finally {
                SimpleConnetionPool.pushConnectionBackToPool(conn);
            }
        }finally {
            //最后关闭所有数据库链接，返回关闭链接的总数
            SimpleConnetionPool.close();
        }

        //全部成功
             status.setAck(AckCodeType.Success);
             response.setResponseStatus(status);
             response.setTransportId(transportID);
             response.setResultMessage("FtpPath:"+dirField+";groupID:"+groupID+";pageName:"+pageName+";timeStamp:"+timeStamp+"; Success!");
        return response;
    }

    //得到唯一标识
    public String getGuid(){
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.replace("-","");
        return uuid ;
    }

    //设置ErrorMessage并返回response
    public PicGetTransportResponseType getErrorResponse(String errorMess){
        PicGetTransportResponseType response = new PicGetTransportResponseType();
        ResponseStatusType status = new ResponseStatusType();
        ArrayList<ErrorDataType> errors = new ArrayList<ErrorDataType>();
        status.setAck(AckCodeType.Failure);
        ErrorDataType errorData = new ErrorDataType();
        errorData.setMessage(errorMess);
        errors.add(errorData);
        status.setErrors(errors);
        response.setResponseStatus(status);
        response.setResultMessage(errorMess);
        return response;
    }

    //新建文件路径
    public boolean createDir(String Path){
        File file = new File(Path);
        if (file.exists()){
            System.out.println("目录已经存在！");
            return true;
        }

        if (!Path.endsWith(File.separator)){// 结尾是否以"/"结束
            Path = Path+ File.separator;
            System.out.println("新建目录，修复路径："+Path);
        }

        if (file.mkdir()){
            System.out.println("创建目录成功");
            return true;
        }

        return false;
    }
}
