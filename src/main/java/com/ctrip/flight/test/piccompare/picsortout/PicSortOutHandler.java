package com.ctrip.flight.test.piccompare.picsortout;

import com.ctrip.flight.framework.soa.entities.ServiceContext;
import com.ctrip.flight.framework.soa.entities.ServiceHandler;
import com.ctrip.flight.test.connection.SimpleConnetionPool;
import com.ctrip.flight.test.piccompare.common.ConfigKeys;
import com.ctrip.flight.test.piccompare.common.ConfigValues;
import com.ctrip.flight.test.util.RemoveFile;
import com.ctriposs.baiji.rpc.common.types.AckCodeType;
import com.ctriposs.baiji.rpc.common.types.ErrorDataType;
import com.ctriposs.baiji.rpc.common.types.ResponseStatusType;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by yanqx on 2017/8/23.
 */
public class PicSortOutHandler implements ServiceHandler<PicSortOutResponseType> {
    @Override
    public PicSortOutResponseType process(ServiceContext serviceContext) throws Exception {
        PicSortOutRequestType request =  (PicSortOutRequestType)serviceContext.getRequest();
        PicSortOutResponseType response = new PicSortOutResponseType();
        ResponseStatusType status = new ResponseStatusType();
        //初始化需要的配置
        ConfigValues initConfig = new ConfigValues(ConfigKeys.FTP_PATH);
        String dirField = ConfigValues.configs.get(ConfigKeys.FTP_PATH);

        String TransportId = request.getTransportId();
        if (TransportId==null||TransportId.length()==0){
            return getErrorResponse("The transportid is null ! ");
        }

        //判断TransportId是否在数据库中存在，如果存在检查状态，如果状态为DOING则不作处理，如果状态为TODO或者DONE则进行整理，并更新数据库状态
        new SimpleConnetionPool("jdbc:sqlserver://10.2.33.162\\TestAutoServer:8765;databaseName=FlightBookingAutomation","sa","Admin_001");
        Connection conn = SimpleConnetionPool.getConnection();
        Statement stmt = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String Filestatus ="";
        String SelectSQL = "select status from FlightBookingAutomation..ImageTransport where transportId = ?";
        String UpdateSQL = "update FlightBookingAutomation..ImageTransport set status= ? where transportId=?";

        try {
                if (conn!=null) {
                    pstmt = conn.prepareStatement(SelectSQL);
                    pstmt.setString(1,TransportId);
                    rs= pstmt.executeQuery();
                    if (rs.next()){
                        Filestatus = rs.getString(1);
                        if(Filestatus.equals("TODO")){
                            pstmt = conn.prepareStatement(UpdateSQL);
                            pstmt.setString(1,"DOING");
                            pstmt.setString(2,TransportId);
                            int updateCount= pstmt.executeUpdate();
                            if(updateCount>0){
                                //进行整理并更新status为DOING
                                boolean removeResult = cutFileByTransportID(TransportId,dirField);
                                if(removeResult){
                                    pstmt.setString(1,"DONE");
                                    pstmt.setString(2,TransportId);
                                    pstmt.executeUpdate();
                                }else{
                                    //更新失败，恢复状态
                                    pstmt.setString(1,"TODO");
                                    pstmt.setString(2,TransportId);
                                    pstmt.executeUpdate();
                                    return getErrorResponse("remove file failed");
                                }
                            }else{
                                return getErrorResponse("update status failed!");
                            }
                        }else{
                            return getErrorResponse("The Status is "+Filestatus+", we will do nothing !");
                        }

                    }else{
                        return getErrorResponse("The transportid is not exist ! ");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }finally {
                SimpleConnetionPool.pushConnectionBackToPool(conn);
                SimpleConnetionPool.close();
            }

            status.setAck(AckCodeType.Success);
            response.setResponseStatus(status);
            response.setResultMessage("the files of TransportId: "+TransportId+" have been removed successfully!");

        return response;
    }

    //通过TransportID进行文件剪切操作
    public boolean cutFileByTransportID(String TransportID,String sourcePath){
        File file = new File(sourcePath);
        ArrayList<String> sourceFilePathList = new ArrayList<>();
        String destPath =sourcePath+"\\"+TransportID;
        if (!file.exists()){
            System.out.println(sourcePath+"not exists");
            return false;
        }
        File [] files = file.listFiles();
        for (int i = 1;i<files.length;i++){
            File currentFile = files[i];
            if (currentFile.isDirectory()) {
                continue;
            }
            String fileName = currentFile.getName();
            if(fileName.startsWith(TransportID)){
                sourceFilePathList.add(currentFile.getAbsolutePath());
            }
        }
        if (sourceFilePathList.size()!=0){
            for (int i=0;i<sourceFilePathList.size();i++){
                RemoveFile.cutfile(sourceFilePathList.get(i),destPath);
            }
        }else{
            System.out.println("无符合要求的文件！");
        }
        return true;
    }

    //设置ErrorMessage并返回response
    public PicSortOutResponseType getErrorResponse(String errorMess){
        PicSortOutResponseType response = new PicSortOutResponseType();
        ResponseStatusType status = new ResponseStatusType();
        ArrayList<ErrorDataType> errors = new ArrayList<ErrorDataType>();
        status.setAck(AckCodeType.Failure);
        ErrorDataType errorData = new ErrorDataType();
        errorData.setMessage(errorMess);
        errors.add(errorData);
        status.setErrors(errors);
        response.setResponseStatus(status);
        response.setResultMessage(errorMess);
        return response;
    }


}
