package com.ctrip.flight.test.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by yanqx on 2017/8/24.
 */
public class RemoveFile {
    public static boolean cutfile(String source,String destiny) {
        int s_mid = source.lastIndexOf("\\");
        String s_last = source.substring(s_mid+1);//文件名
        String end = destiny +"\\" +s_last ;//新文件绝对路径

        String source_root = source.substring(0, s_mid);//原文件根目录

        File destiny_file = new File(end); //新文件绝对路径
        File source_file = new File(source);//旧文件绝对路径

        if(source_file.exists())
        {
            if(destiny.equals(source_root))
            {
                System.out.println("目标文件夹和源文件夹相同！");
            }
            else if(destiny_file.exists())
            {
                System.out.println("目标文件夹已经存在该文件");
                end = destiny+"/(副本)"+s_last;//重名文件建立副本
                try {
                    FileCopeCut(source,end);
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            else
            {
                File destiny_root = new File(destiny);
                if(!destiny_root.exists()){
                    System.out.println("创建新文件夹");
                    destiny_root.mkdirs();
                }
                try {
                    FileCopeCut(source,end);
                    return true;
                } catch (IOException e) {
                    return false;
                }
            }
        }
        else{System.out.println("源文件不存在");}
        return false;
    }

    //剪切文件输入输出流
    private static void FileCopeCut(String source,String end) throws IOException {
        FileInputStream f_in = null;
        FileOutputStream f_out = null;
        try {
            f_in = new FileInputStream(source);
            f_out = new FileOutputStream(end);
            byte[] b = new byte[2048];
            int length = f_in.read(b);
            while(length > 0){
                f_out.write(b, 0, length);
                length = f_in.read(b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            if (f_in!=null){f_in.close();}
            if ( f_out!=null){f_out.close();}
        }
        File endFile = new File(end);
        if(endFile.exists()){
            System.out.println("新文件就绪");
        }
        File del = new File(source);
        if (del.exists()){
            del.delete();
            System.out.println("剪切已完成");
        }
    }
}
